**Real Time Form Validation**

Just a small form validation page written in pure javaScript.
 
Added some CSS for flat UI look.

Definitely missing several standard validations, but works as an example/template.

![demo1.png](https://bitbucket.org/repo/Rz87k9/images/841103039-demo1.png)